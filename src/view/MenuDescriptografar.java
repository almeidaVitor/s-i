package view;

import java.util.Scanner;

public class MenuDescriptografar {

	public void apresentarMenuDescrip(){
		
		Scanner ler = new Scanner(System.in);
		int opcao;
		
		do {
			System.out.println("---- MENU DESCRIPTOGRAFAR ----"
					+"\n1 Cifra de Cesar"
					+"\n2 DES"
					+"\n3 AES"
					+"\n4 Sair");
			
			opcao = ler.nextInt();
			
			switch (opcao) {
			case 1:
				//CifraCesar
				this.CifraCesarDesc();
				break;
			case 2:
				//DES
				this.DESDesc();
				break;
			case 3:
				//AES
				this.AESDesc();
				break;
			case 4:
				//Sair
				Menu menu = new Menu();
				menu.apresentarMenu();
				break;
			default:
				System.out.println("Op��o inv�lida!");
			}
		}
		while (opcao != 4);		
		ler.close();
	}

	private void CifraCesarDesc() {
		System.out.println("---- DESCRIPTOGRAFAR COM CIFRA DE CESAR ----");
		System.out.println("Digite a palavra a ser criptografada: ");
		System.out.println("Digite a chave: ");
		
	}

	private void DESDesc() {
		System.out.println("---- DESCRIPTOGRAFAR COM DES ----");
		System.out.println("Digite a palavra a ser criptografada: ");
		System.out.println("Digite a chave: ");
		
	}
	
	private void AESDesc() {
		System.out.println("---- DESCRIPTOGRAFAR COM AES ----");
		System.out.println("Digite a palavra a ser criptografada: ");
		System.out.println("Digite a chave: ");		
	}
	
}
