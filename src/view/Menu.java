package view;

import java.util.Scanner;

public class Menu {
	
	public void apresentarMenu(){
		
		Scanner ler = new Scanner(System.in);
		int opcao;
		
		do {
			System.out.println("---- SEGURAN�A DA INFORMA��O ----"
					+ "\nDigite a op��o desejada:"
					+ "\n1 Criptografar"
					+ "\n2 Descriptografar"
					+ "\n3 Sair");
			
			opcao = ler.nextInt();
			
			switch (opcao) {
			case 1:
				//Criptografar
				MenuCriptografar menuCrip = new MenuCriptografar();
				menuCrip.apresentarMenuCrip();
				break;
			case 2:
				//Descriptografar
				MenuDescriptografar menuDescrip = new MenuDescriptografar();
				menuDescrip.apresentarMenuDescrip();
				break;
			case 3:
				System.exit(0);
				break;
			default:
				System.out.println("Op��o inv�lida!");
			}
		}
		while (opcao != 3);
		ler.close();
				
	}

}
