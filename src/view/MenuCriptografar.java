package view;

import controller.Controladora;
import java.time.DayOfWeek;
import java.util.Scanner;

public class MenuCriptografar {
    Scanner ler = new Scanner(System.in);
    Controladora con = new Controladora();
    
	
	public void apresentarMenuCrip(){
		
    		
		int opcao;
		
		do {
			System.out.println("---- MENU CRIPTOGRAFAR ----"
					+"\n1 Cifra de Cesar"
					+"\n2 DES"
					+"\n3 AES"
					+"\n4 Sair");
			
			opcao = ler.nextInt();
			
			switch (opcao) {
			case 1:
				//CifraDeCesar
				this.cifraCesarCrip();
				break;
			case 2:
				//DES
				this.DESCrip();
				break;
			case 3:
				//AES
				this.AESCrip();
				break;
			case 4:
				//Sair
				Menu menu = new Menu();
				menu.apresentarMenu();
				break;
			default:
				System.out.println("Opção inválida!");
			}
		}
		while (opcao != 4);
		ler.close();
				
	}
	
	private void cifraCesarCrip() {
		System.out.println("---- CRIPTOGRAFAR COM CIFRA DE CESAR ----");
		System.out.println("Digite a palavra a ser criptografada: ");
                String palavra = ler.nextLine();
		System.out.println("Digite a chave: ");
		int chave = ler.nextInt();
                con.criptografarCesar(palavra, chave);
               
                
	}
	
	private void DESCrip() {
		System.out.println("---- CRIPTOGRAFAR COM DES ----");
		System.out.println("Digite a palavra a ser criptografada: ");
                String palavra = ler.nextLine();
		System.out.println("Digite a chave: ");
                int chave = ler.nextInt();
                //con.criptografarDES(palavra, chave);
		
	}
	
	private void AESCrip() {
		System.out.println("---- CRIPTOGRAFAR COM AES ----");
		System.out.println("Digite a palavra a ser criptografada: ");
		System.out.println("Digite a chave: ");
		
	}


}
