package model;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class AES {
	
	public static Key getChave(String chave){
		Key chaveAES = new SecretKeySpec(chave.getBytes(), "AES");
		return chaveAES;
	}
	
	public static String encriptar(Key chave, String texto){
		Cipher cifradorAES = null;
		byte[] textoPuro = null;
		String textoCifrado = null;
		try{
			cifradorAES = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cifradorAES.init(Cipher.ENCRYPT_MODE, chave);
			textoPuro = texto.getBytes("UTF-8");
			textoCifrado = Base64.getEncoder().encodeToString(cifradorAES.doFinal(textoPuro));
		} catch (UnsupportedEncodingException e ){
			System.out.println("Erro de Encoding.");
		} catch (NoSuchAlgorithmException e){
			System.out.println("Erro durante o processo do altoritimo do AES.");
		} catch (NoSuchPaddingException e){
			System.out.println("Erro durante o proesso de preenchimento(padding) do AES.");
		} catch (InvalidKeyException e){
			System.out.println("Erro de chave invalida do AES.");
		} catch (IllegalBlockSizeException e){
			System.out.println("Erro de tamanho de bloco ilegal do AES.");
		} catch (BadPaddingException e){
			System.out.println("Erro de preenchimento (Padding) mal fomatado.");
		}
		return (textoCifrado);
        }
        
	public static String descriptografar(Key chave, String texto){
		Cipher descriptarAES = null;
		byte[] textoCifrado = null;
		String textoDescriptografado = null;
		try{
			descriptarAES = Cipher.getInstance("DES/ECB/PKCS5Padding");
			descriptarAES.init(Cipher.DECRYPT_MODE, chave);
			textoCifrado = texto.getBytes("UTF-8");
			byte[] textoDecriptografado = descriptarAES.doFinal(textoCifrado);
		} catch (UnsupportedEncodingException e ){
			System.out.println("Erro de Encoding.");
		} catch (NoSuchAlgorithmException e){
			System.out.println("Erro durante o processo do altoritimo do AES.");
		} catch (NoSuchPaddingException e){
			System.out.println("Erro durante o proesso de preenchimento(padding) do AES.");
		} catch (InvalidKeyException e){
			System.out.println("Erro de chave invalida do AES.");
		} catch (IllegalBlockSizeException e){
			System.out.println("Erro de tamanho de bloco ilegal do AES.");
		} catch (BadPaddingException e){
			System.out.println("Erro de preenchimento (Padding) mal fomatado.");
		}
		return (textoDescriptografado);
	}
		
	}