package model;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class DES {
	
	public static Key getChave(String chave){
		Key chaveDES = new SecretKeySpec(chave.getBytes(), "DES");
		return chaveDES;
	}
	
	public static String encriptar(Key chave, String texto){
		Cipher cifradorDES = null;
		byte[] textoPuro = null;
		String textoCifrado = null;
		try{
			cifradorDES = Cipher.getInstance("DES/ECB/PKCS5Padding");
			cifradorDES.init(Cipher.ENCRYPT_MODE, chave);
			textoPuro = texto.getBytes("UTF-8");
			textoCifrado = Base64.getEncoder().encodeToString(cifradorDES.doFinal(textoPuro));
		} catch (UnsupportedEncodingException e ){
			System.out.println("Erro de Encoding.");
		} catch (NoSuchAlgorithmException e){
			System.out.println("Erro durante o processo do altoritimo do DES.");
		} catch (NoSuchPaddingException e){
			System.out.println("Erro durante o proesso de preenchimento(padding) do DES.");
		} catch (InvalidKeyException e){
			System.out.println("Erro de chave invalida do DES.");
		} catch (IllegalBlockSizeException e){
			System.out.println("Erro de tamanho de bloco ilegal do DES.");
		} catch (BadPaddingException e){
			System.out.println("Erro de preenchimento (Padding) mal fomatado.");
		}
		return (textoCifrado);
	}
		
	public static String descriptografar(Key chave, String texto){
		Cipher descriptarDES = null;
		byte[] textoCifrado = null;
		String textoDescriptografado = null;
		try{
			descriptarDES = Cipher.getInstance("DES/ECB/PKCS5Padding");
			descriptarDES.init(Cipher.DECRYPT_MODE, chave);
			textoCifrado = texto.getBytes("UTF-8");
			byte[] textoDecriptografado = descriptarDES.doFinal(textoCifrado);
		} catch (UnsupportedEncodingException e ){
			System.out.println("Erro de Encoding.");
		} catch (NoSuchAlgorithmException e){
			System.out.println("Erro durante o processo do altoritimo do DES.");
		} catch (NoSuchPaddingException e){
			System.out.println("Erro durante o proesso de preenchimento(padding) do DES.");
		} catch (InvalidKeyException e){
			System.out.println("Erro de chave invalida do DES.");
		} catch (IllegalBlockSizeException e){
			System.out.println("Erro de tamanho de bloco ilegal do DES.");
		} catch (BadPaddingException e){
			System.out.println("Erro de preenchimento (Padding) mal fomatado.");
		}
		return (textoDescriptografado);
	}
		
	}

